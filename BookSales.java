import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.lang.*;
import java.util.Map.*;

public class BookSales {

	public static final String delimiter = ",";
  public static HashMap<String, Integer> bookidandsell = new HashMap<String, Integer>();
  public static Map<String, Integer> topsellingbook = new LinkedHashMap<String, Integer>();
  public static Map<String, Integer> customerandpurchase = new HashMap<String, Integer>();
  public static Map<String, Integer> topcustomers = new LinkedHashMap<String, Integer>();
  public static Map<String, Float> bookidandprice = new HashMap<String, Float>();
  public static Map<String, Float> totalsaleamountondate = new HashMap<String, Float>();

  // method to read and process bookslist.csv - Commented By Mihir
	public static void readBookListCsv(String csvFile) {

		try {
			File file = new File(csvFile);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";

			String[] tempArr;
			while ((line = br.readLine()) != null)
      {
				tempArr = line.split(delimiter);
        int itrcount = 0;
        String bookid ="";
        float bookprice = 0.00f;
				for (String tempStr : tempArr)
        {
            //  Older Code
            //  System.out.print(tempStr + " ");
            //  if else is added due to check for last iteration to avoid last unnecessary comma -  Commented By Mihir

            // Here we save Bookn id to obtain appropriate K-V map - Commented By Mihir
            if (itrcount == 0)
            {
                bookid = tempStr;
            }

            if (tempStr == tempArr[tempArr.length-1])
            {
                System.out.print(tempStr + " ");
                // Here we save Bookn id to obtain appropriate K-V map - Commented By Mihir
                bookprice = Float.parseFloat(tempStr);
            }
            else
            {
                System.out.print(tempStr + ",  ");
            }

            itrcount++;
				}

        // Here we generate bookid - price | K-V map - Commented By Mihir
        bookidandprice.put(bookid,bookprice);
        System.out.println();
			}

			br.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

  // method to read and process saleslist.csv - Commented By Mihir
	public static void readSalesListCsv(String csvFile) {

		try {
			File file = new File(csvFile);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";

			String[] tempArr;
      String[] salesRow;
			while ((line = br.readLine()) != null)
      {

				tempArr = line.split(delimiter);
        int itrcount = 0;
        String customeremail="";
        int custpurchasecount = 0;
        String saledate = "";
        Float totalsaleondate = 0.00f;
				for (String tempStr : tempArr)
        {
            //  Older Code
            //  System.out.print(tempStr + " ");
            //  if else is added due to check for last iteration to avoid last unnecessary comma -  Commented By Mihir
            if (tempStr == tempArr[tempArr.length-1])
            {
                System.out.print(tempStr + " ");

                if(itrcount == 0)
                {
                    saledate = tempStr;
                }

                // Here we get customer email to save in map - Commented By Mihir
                if (itrcount == 1)
                {
                    //save into temporary varible - Commented By Mihir
                    customeremail = tempStr;
                }

                // Here we get purchase count by customer to save in map - Commented By Mihir
                if (itrcount == 3)
                {
                    //save into temporary varible - Commented By Mihir
                    custpurchasecount = Integer.parseInt(tempStr);
                }

                //Here we separate bookid and its sell and convert into K-V pair to find list of top selling book - Commented By Mihir
                if (itrcount>3)
                {

                    //Here we separate bookid and its sell count by spliting from semicolon - Commented By Mihir
                    String[] bookidandcount = tempStr.split(";");
                    //Here we convert book sell count into integer to help in sorting - Commendted By Mihir
                    int booksellcount = Integer.parseInt(bookidandcount[1]);
                    //Here we check if book id exist already or not  - Commented By Mihir
                    if (bookidandsell.containsKey(bookidandcount[0])) {

                        //Here if bookid exist then we need to update its sell count - Commented By Mihir
                        booksellcount = booksellcount + bookidandsell.get(bookidandcount[0]);
                    }
                    //add bookid and sell count into map K-V pair - Commented By Mihir
                    bookidandsell.put(bookidandcount[0],booksellcount);

                    //--------------------------------------------------------------------------//
                    //Here i started to calculate total sale on date - Commented By Mihir
                    totalsaleondate = totalsaleondate + (booksellcount*bookidandprice.get(bookidandcount[0]));
                   //--------------------------------------------------------------------------//
                }


            }
            else
            {
                System.out.print(tempStr + ",  ");

                if(itrcount == 0)
                {
                    saledate = tempStr;
                }

                // Here we get customer email to save in map - Commented By Mihir
                if (itrcount == 1)
                {
                    //save into temporary varible - Commented By Mihir
                    customeremail = tempStr;
                }

                // Here we get purchase count by customer to save in map - Commented By Mihir
                if (itrcount == 3)
                {
                    //save into temporary varible - Commented By Mihir
                    custpurchasecount = Integer.parseInt(tempStr);
                }


                //Here we separate bookid and its sell and convert into K-V pair to find list of top selling book - Commented By Mihir
                if (itrcount>3)
                {
                    //Here we separate bookid and its sell count by spliting from semicolon - Commented By Mihir
                    String[] bookidandcount = tempStr.split(";");
                    //Here we convert book sell count into integer to help in sorting - Commendted By Mihir
                    int booksellcount = Integer.parseInt(bookidandcount[1]);
                    //Here we check if book id exist already or not  - Commented By Mihir
                    if (bookidandsell.containsKey(bookidandcount[0]))
                    {
                        //Here if bookid exist then we need to update its sell count - Commented By Mihir
                        booksellcount = booksellcount + bookidandsell.get(bookidandcount[0]);
                    }
                    //add bookid and sell count into map K-V pair - Commented By Mihir
                    bookidandsell.put(bookidandcount[0],booksellcount);


                    //--------------------------------------------------------------------------//
                    //Here i started to calculate total sale on date - Commented By Mihir
                    totalsaleondate = totalsaleondate + (booksellcount*bookidandprice.get(bookidandcount[0]));
                    //--------------------------------------------------------------------------//

                }
            }

            itrcount++;
				}

        //Here we check if customer exist already - Commented By Mihir
        if (customerandpurchase.containsKey(customeremail))
        {
            //Here we update purchase count by customer - Commented By Mihir
            custpurchasecount = custpurchasecount + customerandpurchase.get(customeremail);
        }

        //Add customer and purchase count in map - Commented By Mihir
        customerandpurchase.put(customeremail,custpurchasecount);

        // Here we check if data exist already - Commented By Mihir
        if (totalsaleamountondate.containsKey(saledate))
        {
            //Here we Update Sale amount due to date exist - Commented By Mihir
            totalsaleondate = totalsaleondate + totalsaleamountondate.get(saledate);
        }
        totalsaleamountondate.put(saledate,totalsaleondate);
        System.out.println();
			}

			br.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

 //Method to find Best Selling Book - Commented By Mihir
  public static void findTopSellingBook(){

    //convert map to a List
        List<Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(bookidandsell.entrySet());

        //sorting the list with a comparator
        Collections.sort(list, new Comparator<Entry<String, Integer>>() {
          public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
            return (o2.getValue()).compareTo(o1.getValue());
          }
        });

        //convert sortedMap back to Map

        for (Entry<String, Integer> entry : list) {
          topsellingbook.put(entry.getKey(), entry.getValue());
        }

  }

  //Method to find Top Customer Based on purchase made by them - Commented By Mihir
  public static void findTopCustomers(){

    //convert map to a List
        List<Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(customerandpurchase.entrySet());

        //sorting the list with a comparator
        Collections.sort(list, new Comparator<Entry<String, Integer>>() {
          public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
            return (o2.getValue()).compareTo(o1.getValue());
          }
        });

        //convert sortedMap back to Map

        for (Entry<String, Integer> entry : list) {
          topcustomers.put(entry.getKey(), entry.getValue());
        }

  }


	public static void main(String[] args) {
		// csv file to read

    //String csvFileBookList = "C:/Users/Mihir/Desktop/bookslist.csv";
    //String csvFileSalesList = "C:/Users/Mihir/Desktop/saleslist.csv";



    Scanner in = new Scanner(System.in);

    System.out.print("Enter Books Csv Path =");
    String csvFileBookList = in.nextLine();
    System.out.println();
    System.out.print("Enter Sales Csv Path =");
    String csvFileSalesList = in.nextLine();
		System.out.println();
    BookSales.readBookListCsv(csvFileBookList);
    BookSales.readSalesListCsv(csvFileSalesList);
    System.out.println("-------------------------------------------------------------------");
    System.out.println("-------------------------------------------------------------------");
    System.out.println("-------------------------------------------------------------------");
    System.out.println("-------------------------------------------------------------------");
    System.out.println("-------------------------------------------------------------------");
    System.out.println();
    System.out.println();
    System.out.println();
    System.out.println();
    System.out.println();

    System.out.print("Are you want to display book.list?(yes or no) =");
    String dspbooklist = in.nextLine();
    if (dspbooklist.equals("yes")) {
      System.out.println("-------------------------------------------------------------------");
      System.out.println("Book List");
      System.out.println("-------------------------------------------------------------------");
      BookSales.readBookListCsv(csvFileBookList);
      System.out.println("-------------------------------------------------------------------");
      System.out.println("-------------------------------------------------------------------");
    }
    System.out.println();

    System.out.print("Are you want to display sales.list?(yes or no) =");
    String dspsaleslist = in.nextLine();
    if (dspsaleslist.equals("yes")) {
      System.out.println("-------------------------------------------------------------------");
      System.out.println("Sales List");
      System.out.println("-------------------------------------------------------------------");
      BookSales.readSalesListCsv(csvFileSalesList);
      System.out.println("-------------------------------------------------------------------");
      System.out.println("-------------------------------------------------------------------");
    }
    System.out.println();

    System.out.println("Top selling Books");
    System.out.print("Enter F for full list or enter number=");
    String dsptopsellingbooks = in.nextLine();
    System.out.println("-------------------------------------------------------------------");
    if (dsptopsellingbooks.equals("f")) {
      BookSales.findTopSellingBook();
      System.out.println("List of Top Selling Books:");
        System.out.println("-------------------------------------------------------------------");
        for (Map.Entry<String, Integer> en : topsellingbook.entrySet()) {
                System.out.println("Book Id = " + en.getKey() +
                              ", Total Sell for this Book = " + en.getValue());
            }
        System.out.println("-------------------------------------------------------------------");
        System.out.println("-------------------------------------------------------------------");

    }
    else{

      Integer count = Integer.parseInt(dsptopsellingbooks);
      BookSales.findTopSellingBook();
      System.out.println("Top Selling Books:");
        System.out.println("-------------------------------------------------------------------");
        for (Map.Entry<String, Integer> en : topsellingbook.entrySet()) {
                System.out.println("Book Id = " + en.getKey() +
                              ", Total Sell for this Book = " + en.getValue());
              if (count == 1) {
                break;
              }
              count--;
            }
        System.out.println("-------------------------------------------------------------------");
        System.out.println("-------------------------------------------------------------------");
    }
    System.out.println();


    System.out.println("Top Customers");
    System.out.print("Enter F for full list or enter number=");
    String dsptopcustomer = in.nextLine();
    System.out.println("-------------------------------------------------------------------");
    if (dsptopcustomer.equals("f")) {
      BookSales.findTopCustomers();
      System.out.println("List of Top Customers:");
      System.out.println("-------------------------------------------------------------------");
      for (Map.Entry<String, Integer> en : topcustomers.entrySet()) {
              System.out.println("Customer Email = " + en.getKey() +
                            ", Total Purchase By Customer = " + en.getValue());
          }
      System.out.println("-------------------------------------------------------------------");
      System.out.println("-------------------------------------------------------------------");

    }
    else{

        Integer count = Integer.parseInt(dsptopcustomer);
        BookSales.findTopCustomers();
        System.out.println("Top Customers:");
        System.out.println("-------------------------------------------------------------------");
        for (Map.Entry<String, Integer> en : topcustomers.entrySet()) {
                System.out.println("Customer Email = " + en.getKey() +
                              ", Total Purchase By Customer = " + en.getValue());

                if (count == 1) {
                      break;
                }

                count--;

            }
        System.out.println("-------------------------------------------------------------------");
        System.out.println("-------------------------------------------------------------------");
    }
    System.out.println();


    System.out.print("Enter Date (YYYY-DD-MM) to know sell amount =");
    String dspsellondate = in.nextLine();
    System.out.println("-------------------------------------------------------------------");
    System.out.println("Sale On Date : " +dspsellondate+" = "+totalsaleamountondate.get(dspsellondate));
    System.out.println("-------------------------------------------------------------------");
    System.out.println("-------------------------------------------------------------------");




    //System.out.println("-------------------------------------------------------------------");
    //System.out.println("Book List");
    //System.out.println("-------------------------------------------------------------------");
		//BookSales.readBookListCsv(csvFileBookList);
    //System.out.println("-------------------------------------------------------------------");
    //System.out.println("Sales List");
    //System.out.println("-------------------------------------------------------------------");
    //BookSales.readSalesListCsv(csvFileSalesList);
    //System.out.println("-------------------------------------------------------------------");
    //System.out.println("-------------------------------------------------------------------");
    //System.out.println(bookidandsell);
   //  System.out.println("-------------------------------------------------------------------");
   //  System.out.println("-------------------------------------------------------------------");
   //  BookSales.findTopSellingBook();
   //  System.out.println(topsellingbook);
   //  System.out.println("-------------------------------------------------------------------");
   //  System.out.println("-------------------------------------------------------------------");
  /*  System.out.println("List of Top Selling Books");
    System.out.println("-------------------------------------------------------------------");
    for (Map.Entry<String, Integer> en : topsellingbook.entrySet()) {
            System.out.println("Book Id = " + en.getKey() +
                          ", Total Sell for this Book = " + en.getValue());
        }
    System.out.println("-------------------------------------------------------------------");
    System.out.println("-------------------------------------------------------------------");  */
  //  System.out.println(customerandpurchase);
  //  BookSales.findTopCustomers();
  //  System.out.println("-------------------------------------------------------------------");
  //  System.out.println("-------------------------------------------------------------------");
  /*  System.out.println("List of Top Customers");
      System.out.println("-------------------------------------------------------------------");
      for (Map.Entry<String, Integer> en : topcustomers.entrySet()) {
              System.out.println("Customer Email = " + en.getKey() +
                            ", Total Purchase By Customer = " + en.getValue());
          }
      System.out.println("-------------------------------------------------------------------");
      System.out.println("-------------------------------------------------------------------");  */
    //System.out.println(bookidandprice);
    //System.out.println("-------------------------------------------------------------------");
    //System.out.println("-------------------------------------------------------------------");
    //System.out.println(totalsaleamountondate);




	}

}
